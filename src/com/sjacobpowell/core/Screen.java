package com.sjacobpowell.core;

import com.sjacobpowell.entities.Entity;

public class Screen extends Bitmap {
	private int xRenderTileCount;
	private int yRenderTileCount;

	public Screen(int width, int height) {
		super(width, height);
		xRenderTileCount = width / Game.tileSize.width + 3;
		yRenderTileCount = height / Game.tileSize.height + 3;
	}

	public void render(Game game) {
		clear();
		int xViewPortCenter = width / 2;
		int yViewPortCenter = height / 2;
		drawBackground(game, xViewPortCenter, yViewPortCenter);
		try {
			game.dartMutex.acquire();
			game.darts.values().forEach(dart -> drawObject(dart, game, xViewPortCenter, yViewPortCenter));
			game.dartMutex.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		game.enemies.values().forEach(object -> drawObject(object, game, xViewPortCenter, yViewPortCenter));
		drawPlayer(game, xViewPortCenter, yViewPortCenter);
	}

	private void drawObject(Entity object, Game game, int xViewPortCenter, int yViewPortCenter) {
		if (!object.alive) {
			return;
		}
		double xDifference = game.player.x - object.x;
		double yDifference = game.player.y - object.y;
		if (Math.abs(xDifference) / 2 <= xViewPortCenter && Math.abs(yDifference) / 2 <= yViewPortCenter) {
			double xPlayerDistanceToGameRight = game.width - game.player.x;
			double yPlayerDistanceToGameBottom = game.height - game.player.y;
			double xDartDistanceToGameRight = game.width - object.x;
			double yDartDistanceToGameBottom = game.height - object.y;

			double xOffset = game.player.x < xViewPortCenter ? object.x : xViewPortCenter - xDifference;
			double yOffset = game.player.y < yViewPortCenter ? object.y : yViewPortCenter - yDifference;
			xOffset = xPlayerDistanceToGameRight < xViewPortCenter ? 2 * xViewPortCenter - xDartDistanceToGameRight
					: xOffset;
			yOffset = yPlayerDistanceToGameBottom < yViewPortCenter ? 2 * yViewPortCenter - yDartDistanceToGameBottom
					: yOffset;
			draw(object.sprite, xOffset - object.sprite.width / 2, yOffset - object.sprite.height / 2);
		}
	}

	private void drawPlayer(Game game, int xViewPortCenter, int yViewPortCenter) {
		double xPlayerOffset = xViewPortCenter;
		double yPlayerOffset = yViewPortCenter;
		xPlayerOffset = game.player.x < xViewPortCenter ? game.player.x : xPlayerOffset;
		yPlayerOffset = game.player.y < yViewPortCenter ? game.player.y : yPlayerOffset;
		double xPlayerDistanceToGameRight = game.width - game.player.x;
		double yPlayerDistanceToGameBottom = game.height - game.player.y;
		xPlayerOffset = xPlayerDistanceToGameRight < xViewPortCenter ? 2 * xViewPortCenter - xPlayerDistanceToGameRight
				: xPlayerOffset;
		yPlayerOffset = yPlayerDistanceToGameBottom < yViewPortCenter
				? 2 * yViewPortCenter - yPlayerDistanceToGameBottom : yPlayerOffset;
		draw(game.player.sprite, xPlayerOffset - game.player.sprite.width / 2,
				yPlayerOffset - game.player.sprite.height / 2);
	}

	private void drawBackground(Game game, int xViewPortCenter, int yViewPortCenter) {
		int xPlayerOffset = game.player.getXInt() <= xViewPortCenter ? xViewPortCenter : game.player.getXInt();
		int yPlayerOffset = game.player.getYInt() <= yViewPortCenter ? yViewPortCenter : game.player.getYInt();
		xPlayerOffset = xPlayerOffset >= game.width - xViewPortCenter ? game.width - xViewPortCenter : xPlayerOffset;
		yPlayerOffset = yPlayerOffset >= game.height - yViewPortCenter ? game.height - yViewPortCenter : yPlayerOffset;
		int xStartTile = xPlayerOffset / Game.tileSize.width - xRenderTileCount / 2;
		int yStartTile = yPlayerOffset / Game.tileSize.height - yRenderTileCount / 2;
		int xOffsetIndex = -xRenderTileCount / 2;
		int yOffsetIndex;
		for (int xTile = xStartTile; xTile < xStartTile + xRenderTileCount; xTile++) {
			yOffsetIndex = -yRenderTileCount / 2;
			for (int yTile = yStartTile; yTile < yStartTile + yRenderTileCount; yTile++) {
				if (xTile >= 0 && yTile >= 0 && xTile < game.backgroundTiles.size() - 1
						&& yTile < game.backgroundTiles.get(0).size() - 1) {
					Bitmap tile = game.backgroundTiles.get(xTile).get(yTile);
					int xOffset = xViewPortCenter - xPlayerOffset % Game.tileSize.width
							+ xOffsetIndex * Game.tileSize.width;
					int yOffset = yViewPortCenter - yPlayerOffset % Game.tileSize.height
							+ yOffsetIndex * Game.tileSize.height;
					draw(tile, xOffset, yOffset);
				}
				yOffsetIndex++;
			}
			xOffsetIndex++;
		}
	}
}
