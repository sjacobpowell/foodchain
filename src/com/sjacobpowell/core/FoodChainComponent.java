package com.sjacobpowell.core;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FoodChainComponent extends Canvas implements Runnable {
	private static final long serialVersionUID = 1L;
	public static int WIDTH = 800;
	public static int HEIGHT = 600;
	public static final int SCALE = 1;
	private boolean running = false;
	private Thread thread;
	private Game game;
	private Screen screen;
	private InputManager inputManager;
	private BufferedImage image;
	private int[] pixels;
	private static final String OS = System.getProperty("os.name").toLowerCase();

	public FoodChainComponent() {
		Dimension size = new Dimension(WIDTH * SCALE, HEIGHT * SCALE);
		if (OS.indexOf("win") >= 0) {
			size = new Dimension(WIDTH * SCALE - 10, HEIGHT * SCALE - 10);
		}
		setSize(size);
		setPreferredSize(size);
		setMinimumSize(size);
		setMaximumSize(size);

		game = new Game();
		screen = new Screen(WIDTH, HEIGHT);

		image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();

		inputManager = new InputManager();
		addKeyListener(inputManager);
	}

	@Override
	public void run() {
		int frames = 0;
		int tickCount = 0;
		double FPS = 60.0;
		double unprocessedSeconds = 0;
		double secondsPerTick = 1 / FPS;
		double end = System.nanoTime();

		requestFocus();

		while (running) {
			double start = System.nanoTime();
			double passedTime = start - end;
			end = start;
			if (passedTime < 0)
				passedTime = 0;
			if (passedTime > 100000000)
				passedTime = 100000000;

			unprocessedSeconds += passedTime / 1000000000;

			boolean ticked = false;
			while (unprocessedSeconds > secondsPerTick) {
				tick();
				unprocessedSeconds -= secondsPerTick;
				ticked = true;

				tickCount++;
				if (tickCount % FPS * 6 == 0) {
					System.out.print(frames + " fps, ");
					end += 1000;
					frames = 0;
				}
			}

			if (ticked) {
				render();
				frames++;
			} else {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void tick() {
		game.tick(inputManager.keys);
	}

	private void render() {
		BufferStrategy strategy = getBufferStrategy();
		if (strategy == null) {
			createBufferStrategy(3);
			return;
		}

		screen.render(game);
		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = screen.pixels[i];
		}

		Graphics g = strategy.getDrawGraphics();
		g.drawImage(image, 0, 0, WIDTH * SCALE, HEIGHT * SCALE, null);
		g.dispose();
		strategy.show();
	}

	public void start() {
		if (running)
			return;
		running = true;
		thread = new Thread(this);
		thread.start();
	}

	public void stop() {
		if (!running)
			return;
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static JFrame createSplashScreen() throws IOException {
		Image image = ImageIO.read(FoodChainComponent.class.getResource("/res/background3.jpg"));
		JFrame splash = new JFrame();
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(new JLabel(new ImageIcon(image)), BorderLayout.CENTER);
		splash.setContentPane(panel);
		splash.setSize(image.getWidth(null), image.getHeight(null));
		splash.setLocationRelativeTo(null);
		splash.setUndecorated(true);
		splash.setBackground(new Color(1.0f, 1.0f, 1.0f, 0.0f));
		splash.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		splash.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
					System.exit(0);
			}
		});
		splash.setVisible(true);
		return splash;
	}

	public static void main(String[] args) {
		Image image = null;
		JFrame splash = null;;
		try {
			image = ImageIO.read(FoodChainComponent.class.getResource("/res/player/U.png"));
			splash = createSplashScreen();
		} catch (IOException e) {
			System.err.println("Error loading frame icon: " + e.getMessage());
		}
		JFrame frame = new JFrame("Food Chain");
		JPanel panel = new JPanel(new BorderLayout());
		FoodChainComponent game = new FoodChainComponent();
		if(splash != null) splash.dispose();
		panel.add(game, BorderLayout.CENTER);
		if (image != null)
			frame.setIconImage(image);
		frame.setContentPane(panel);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setVisible(true);
		game.start();
		frame.setTitle("Food Chain - " + game.game.player.ID);
	}
}
