package com.sjacobpowell.core;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Semaphore;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sjacobpowell.entities.Dart;
import com.sjacobpowell.entities.Entity;
import com.sjacobpowell.entities.Player;
import com.sjacobpowell.web.client.FoodChainClient;

public class Game {
	public int time;
	public int width;
	public int height;
	public Player player;
	public Map<String, Entity> enemies;
	public Map<String, Entity> darts;
	public List<List<Bitmap>> backgroundTiles;
	public static final Dimension tileSize = new Dimension(200, 200);
	private FoodChainClient client;
	public Semaphore dartMutex = new Semaphore(1);
	private int dartRechargeCount = 0;
	private int dartCount = 0;

	public Game() {
		Bitmap background = ArtTools.getBitmap("/res/background.jpg");
		this.width = background.width;
		this.height = background.height;
		enemies = new HashMap<String, Entity>();
		darts = new HashMap<String, Entity>();
		resetPlayer();
		initializeTiles(background);
		// player.ID = JOptionPane.showInputDialog(null, "Username", "Enter a
		// username:", JOptionPane.QUESTION_MESSAGE);
		player.ID = player.ID == null || player.ID.isEmpty()
				? "Player" + System.nanoTime() + getIP().replaceAll("\\.", "") : player.ID;
		while (client == null) {
			try {
				client = new FoodChainClient(player);
			} catch (ConnectException e) {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					e.printStackTrace();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		new Thread(() -> tickObjects()).start();
	}

	private String getIP() {
		try {
			return new BufferedReader(new InputStreamReader(new URL("http://checkip.amazonaws.com").openStream()))
					.readLine();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Failed to generate a username.");
		}
	}

	private void initializeTiles(Bitmap background) {
		backgroundTiles = new ArrayList<List<Bitmap>>();
		for (int x = 0; x <= background.width / tileSize.width; x++) {
			backgroundTiles.add(new ArrayList<Bitmap>());
			for (int y = 0; y <= background.height / tileSize.height; y++) {
				backgroundTiles.get(x).add(ArtTools.getSubBitmap(background, x * tileSize.width, y * tileSize.height,
						tileSize.width, tileSize.height));
			}
		}
	}

	private void resetPlayer() {
		player = new Player();
		player.x = width / 2;
		player.y = height / 2;
	}

	public void tick(boolean[] keys) {
		if (keys[KeyEvent.VK_ESCAPE])
			System.exit(0);
		boolean up = keys[KeyEvent.VK_UP];
		boolean down = keys[KeyEvent.VK_DOWN];
		boolean left = keys[KeyEvent.VK_LEFT];
		boolean right = keys[KeyEvent.VK_RIGHT];
		boolean dartUp = keys[KeyEvent.VK_W];
		boolean dartDown = keys[KeyEvent.VK_S];
		boolean dartLeft = keys[KeyEvent.VK_A];
		boolean dartRight = keys[KeyEvent.VK_D];
		boolean playerChanged = up || down || left || right;

		tickPlayer(up, down, left, right);

		try {
			dartMutex.acquire();
			fireDart(dartUp, dartDown, dartLeft, dartRight);
			tickDarts();
			keys[KeyEvent.VK_W] = keys[KeyEvent.VK_S] = keys[KeyEvent.VK_A] = keys[KeyEvent.VK_D] = false;
			dartMutex.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		checkCollisions();

		if (playerChanged) {
			client.send((Entity) player);
		}

		try {
			dartMutex.acquire();
			darts.values().forEach(client::send);
			dartMutex.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		time++;
	}

	private void tickObjects() {
		JsonParser parser = new JsonParser();
		Gson gson = new Gson();
		while (true) {
			String received = client.receive();
			JsonObject entityJson = parser.parse(received).getAsJsonObject();
			String ID = entityJson.get("ID").getAsString();
			if (ID.contains("dart") && !ID.contains(player.ID)) {
				try {
					dartMutex.acquire();
					if (entityJson.get("alive").getAsBoolean()) {
						darts.put(ID, gson.fromJson(entityJson, Entity.class));
					} else {
						darts.remove(ID);
					}
					dartMutex.release();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else if (!ID.equalsIgnoreCase(player.ID)) {
				if (entityJson.get("alive").getAsBoolean()) {
					enemies.put(ID, gson.fromJson(entityJson, Entity.class));
				} else {
					enemies.remove(ID);
				}
			}
		}
	}

	private void checkCollisions() {
		try {
			dartMutex.acquire();
			darts.values().forEach(dart -> {
				if (player.isColliding(dart) && !dart.ID.contains(player.ID)) {
					player.alive = false;
				}
			});
			dartMutex.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		enemies.values().forEach(enemy -> {
			if (player.isColliding(enemy)) {
				// System.out.println("Enemy");
			}
		});
	}

	private void tickDarts() {
		darts.values().forEach(Entity::move);
		for (Iterator<Entry<String, Entity>> it = darts.entrySet().iterator(); it.hasNext();) {
			Entry<String, Entity> entry = it.next();
			if (!entry.getValue().alive) {
				it.remove();
			} else if (entry.getValue().x < 0 || entry.getValue().y < 0 || entry.getValue().x >= width
					|| entry.getValue().y >= height) {
				darts.get(entry.getKey()).alive = false;
			}
		}
	}

	private void fireDart(boolean dartUp, boolean dartDown, boolean dartLeft, boolean dartRight) {
		try {
			if (dartRechargeCount >= 60 && (dartUp || dartDown || dartLeft || dartRight)) {
				Dart dart = new Dart(dartUp, dartDown, dartLeft, dartRight, player);
				dart.ID = player.ID + "-dart-" + dartCount++;
				darts.put(dart.ID, dart);
				dartRechargeCount = 0;
			}
		} catch (Exception e) {
		}
		dartRechargeCount++;
		dartRechargeCount = dartRechargeCount < 0 ? 0 : dartRechargeCount;
	}

	private void tickPlayer(boolean up, boolean down, boolean left, boolean right) {
		player.tick(up, down, left, right);
		player.x = player.x - player.sprite.width / 2 < 0 ? player.sprite.width / 2 : player.x;
		player.y = player.y - player.sprite.height / 2 < 0 ? player.sprite.height / 2 : player.y;
		player.x = player.x + player.sprite.width / 2 > width ? width - player.sprite.width / 2 : player.x;
		player.y = player.y + player.sprite.height / 2 > height ? height - player.sprite.height / 2 : player.y;
	}

}
