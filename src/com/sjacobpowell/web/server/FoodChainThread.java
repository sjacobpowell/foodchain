package com.sjacobpowell.web.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.Iterator;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.sjacobpowell.entities.Entity;

public class FoodChainThread extends Thread {
	public Socket socket;
	private PrintWriter send;
	private BufferedReader receive;
	private boolean running;
	private FoodChainServer server;
	private Gson gson = new Gson();
	public Entity entity;

	public FoodChainThread(Socket socket, FoodChainServer server) {
		try {
			this.socket = socket;
			this.server = server;
			running = true;
			send = new PrintWriter(socket.getOutputStream(), true);
			receive = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String receive() {
		try {
			return receive.readLine();
		} catch (IOException e) {
			if (e instanceof SocketException) {
				try {
					socket.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			} else {
				e.printStackTrace();
			}
		}
		end();
		return null;
	}

	public void send(String data) {
		send.println(data);
	}

	private void end() {
		System.out.println(entity.ID + " left.");
		server.entities.remove(entity.ID);
		running = false;
	}

	@Override
	public void run() {
		while (running) {
			try {
				entity = gson.fromJson(receive(), Entity.class);
				if (entity == null) {
					running = false;
					break;
				}
				boolean newEntity = !server.entities.containsKey(entity.ID);
				server.mutex.acquire();
				server.entities.put(entity.ID, entity);
				if (newEntity) {
					if (entity.ID.contains("dart")) {
						System.out.println(entity.ID.split("-")[0] + " shot a dart [" + entity.ID + "].");
					} else {
						System.out.println(entity.ID + " joined.");
					}
					server.entities.values().forEach(entity -> server.update(entity));
				} else {
					server.update(entity);
				}
				Entity entity;
				for (Iterator<Entry<String, Entity>> it = server.entities.entrySet().iterator(); it.hasNext();) {
					entity = it.next().getValue();
					if (!entity.alive) {
						if (entity.ID.contains("dart")) {
							System.out.println(entity.ID + " hit a wall.");
						} else {
							System.out.println(entity.ID + " died.");
						}
						it.remove();
					}
				}
				server.mutex.release();
			} catch (InterruptedException e) {
				end();
			}
		}
	}
}
