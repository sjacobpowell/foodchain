package com.sjacobpowell.web.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

import com.google.gson.Gson;
import com.sjacobpowell.entities.Entity;

public class FoodChainServer {
	private ServerSocket socket;
	private List<FoodChainThread> serverThreads;
	public Semaphore mutex;
	public Map<String, Entity> entities;
	private Gson gson = new Gson();
	private boolean running;
	private int port;

	public FoodChainServer(String portString) {
		entities = new HashMap<String, Entity>();
		mutex = new Semaphore(1);
		port = Integer.parseInt(portString);
		try {
			socket = new ServerSocket(port);
			serverThreads = new ArrayList<FoodChainThread>();
			Runtime.getRuntime().addShutdownHook(new Thread(() -> serverThreads.forEach(serverThread -> {
				try {
					serverThread.socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			})));
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	public void start() {
		running = true;
		accept();
	}

	public void update(Entity entity) {
		serverThreads.forEach(serverThread -> serverThread.send(gson.toJson(entity)));
	}

	private void accept() {
		new Thread(() -> {
			while (running) {
				try {
					serverThreads.add(new FoodChainThread(socket.accept(), this));
					serverThreads.get(serverThreads.size() - 1).start();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	public static void main(String[] args) throws IOException {
		if (args.length < 1) {
			System.out.println("Please provide a port");
			return;
		}
		new FoodChainServer(args[0]).start();
	}
}
