package com.sjacobpowell.web.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import com.google.gson.Gson;
import com.sjacobpowell.entities.Entity;

public class FoodChainClient extends Socket {
	private static final String HOST = "localhost";
	private static final int PORT = 12321;
	private PrintWriter send;
	private BufferedReader receive;
	private Gson gson = new Gson();

	public FoodChainClient(Entity entity) throws UnknownHostException, IOException {
		super(HOST, PORT);
		send = new PrintWriter(getOutputStream(), true);
		receive = new BufferedReader(new InputStreamReader(getInputStream()));
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}));
		send(entity);
	}

	public String receive() {
		try {
			return receive.readLine();
		} catch (IOException e) {
			if (e instanceof SocketException) {
				try {
					close();
					System.exit(0);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			} else {
				e.printStackTrace();
			}
		}
		System.exit(1);
		return null;
	}

	public void send(Entity entity) {
		send.println(gson.toJson(entity));
	}
}
