package com.sjacobpowell.entities;

import com.sjacobpowell.core.ArtTools;
import com.sjacobpowell.core.Bitmap;

public class Dart extends Entity {
	private Bitmap U = ArtTools.getBitmap("/res/dart/U.png");
	private Bitmap D = ArtTools.getBitmap("/res/dart/D.png");
	private Bitmap R = ArtTools.getBitmap("/res/dart/R.png");
	private Bitmap L = ArtTools.getBitmap("/res/dart/L.png");
	public double dartSpeed = 10;

	public Dart(boolean dartUp, boolean dartDown, boolean dartLeft, boolean dartRight, Player player) throws Exception {
		init(dartUp, dartDown, dartLeft, dartRight, player);
	}

	public Dart(boolean dartUp, boolean dartDown, boolean dartLeft, boolean dartRight, Player player, double speed) throws Exception {
		dartSpeed = speed;
		init(dartUp, dartDown, dartLeft, dartRight, player);
	}
	
	private void init(boolean dartUp, boolean dartDown, boolean dartLeft, boolean dartRight, Player player) throws Exception {
		if (dartUp) {
			sprite = U;
			ySpeed = -dartSpeed;
		} else if (dartDown) {
			sprite = D;
			ySpeed = dartSpeed;
		} else if (dartLeft) {
			sprite = L;
			xSpeed = -dartSpeed;
		} else if (dartRight) {
			sprite = R;
			xSpeed = dartSpeed;
		} else {
			throw new Exception("Invalid dart instantiation.");
		}
		x = player.x;
		y = player.y;
	}
}
