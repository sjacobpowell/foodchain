package com.sjacobpowell.entities;

public class Player extends Character {
	private static final double WALK_SPEED = 5;

	public void tick(boolean up, boolean down, boolean left, boolean right) {
		ySpeed = up || down ? (up ? -WALK_SPEED : WALK_SPEED) : 0;
		xSpeed = left || right ? (left ? -WALK_SPEED : WALK_SPEED) : 0;
		move();
		setSprite();
	}
}
