package com.sjacobpowell.entities;

import com.sjacobpowell.core.ArtTools;
import com.sjacobpowell.core.Bitmap;

public class Character extends Entity {
	private Bitmap U = ArtTools.getBitmap("/res/player/U.png");
	private Bitmap UR = ArtTools.getBitmap("/res/player/UR.png");
	private Bitmap R = ArtTools.getBitmap("/res/player/R.png");
	private Bitmap DR = ArtTools.getBitmap("/res/player/DR.png");
	private Bitmap D = ArtTools.getBitmap("/res/player/D.png");
	private Bitmap DL = ArtTools.getBitmap("/res/player/DL.png");
	private Bitmap L = ArtTools.getBitmap("/res/player/L.png");
	private Bitmap UL = ArtTools.getBitmap("/res/player/UL.png");

	public Character() {
		sprite = U;
	}

	public void setSprite() {
		boolean up = ySpeed < 0;
		boolean down = ySpeed > 0;
		boolean left = xSpeed < 0;
		boolean right = xSpeed > 0;
		if (up) {
			if (right) {
				sprite = UR;
			} else if (left) {
				sprite = UL;
			} else {
				sprite = U;
			}
		} else if (down) {
			if (right) {
				sprite = DR;
			} else if (left) {
				sprite = DL;
			} else {
				sprite = D;
			}
		} else {
			if (right) {
				sprite = R;
			} else if (left) {
				sprite = L;
			}
		}
	}
}
