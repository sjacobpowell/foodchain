package com.sjacobpowell.entities;

public class Enemy extends Character {

	public void tick() {
		move();
		setSprite();
	}

	@Override
	public void move() {
		x += xSpeed;
		y += ySpeed;
	}
}
